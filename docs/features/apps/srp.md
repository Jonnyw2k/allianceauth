# Ship Replacement

Ship Replacement helps you to organize ship replacement programs (SRP) for your alliance.

![srp](/_static/images/features/apps/srp.png)

## Installation

Add `'allianceauth.srp',` to your `INSTALLED_APPS` list in your auth project's settings file. Run migrations to complete installation.
